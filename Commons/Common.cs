﻿namespace hotel_management_system_with_c_sharp.Commons
{
    public class Common
    {
        public static string GetFilePath(string filename)
    => Path.Combine(Environment.CurrentDirectory.Replace("bin\\Debug\\net6.0", ""), filename);
    }
}
