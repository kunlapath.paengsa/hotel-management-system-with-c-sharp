﻿namespace hotel_management_system_with_c_sharp.Models
{
    public class UserProfile
    {
        public string Room { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Keycard { get; set; }
    }
}