﻿using hotel_management_system_with_c_sharp;

namespace Test
{
    public class Program
    {
        private static void Main(string[] args)
        => new StartUp().Run();
    }
}