using hotel_management_system_with_c_sharp.Commons;
using hotel_management_system_with_c_sharp.Models;
using System.Text;

namespace hotel_management_system_with_c_sharp
{
    public class SystemService
    {
        TextWriter tw = new StreamWriter(Common.GetFilePath("output.txt"));
        private List<string> availableRooms = new();
        private List<UserProfile> userProfiles = new();
        private int initFloor;
        private int initRoom;

        public void GenerateFileResults(string[] contents)
        {
            foreach (var item in contents)
            {
                var command = item.Split(" ");
                switch (command[0])
                {
                    case "create_hotel":
                        ValidationCreateHotel(command[1], command[2]);
                        CreateHotel(initFloor, initRoom);
                        break;
                    case "book":
                        Booking(command[1], command[2], command[3]);
                        break;
                    case "list_available_rooms":
                        GetListAvailableRooms();
                        break;
                    case "checkout":
                        Checkout(command[1], command[2]);
                        break;
                    case "list_guest":
                        GetListGuest();
                        break;
                    case "get_guest_in_room":
                        GetGuestInRoom(command[1]);
                        break;
                    case "list_guest_by_age":
                        GetListGuestByAge(command[1], command[2]);
                        break;
                    case "list_guest_by_floor":
                        GetListGuestByFloor(command[1]);
                        break;
                    case "book_by_floor":
                        BookByFloor(command[1], command[2], command[3]);
                        break;
                    case "checkout_guest_by_floor":
                        CheckoutGuestByFloor(command[1]);
                        break;
                    default:
                        Console.WriteLine($"Missing command [{command[0]}] from : {item}");
                        break;
                }
            }
            tw.Close();
        }

        private void ValidationCreateHotel(string intFloor, string intRoom)
        {
            try
            {
                initFloor = int.Parse(intFloor);
                initRoom = int.Parse(intRoom);
                if (initFloor > 9 || initFloor < 0 || initRoom > 99 || initRoom < 0 || availableRooms.Any())
                {
                    throw new InvalidOperationException($"Can't create hotel. Please recheck your floor or room number.");
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        private void CreateHotel(int floor, int room)
        {
            for (int i = 1; i < floor + 1; i++)
            {
                for (int j = 1; j < room + 1; j++)
                {
                    availableRooms.Add($"{i}{j:D2}");
                }
            }
            tw.WriteLine($"Hotel created with {floor} floor(s), {room} room(s) per floor.");
        }

        private void Booking(string roomId, string username, string strAge)
        {
            try
            {
                var age = int.Parse(strAge);
                if (availableRooms.Contains(roomId))
                {
                    var keycardNumber = BookUserProfile(roomId, username, age);
                    tw.WriteLine(Massage(new List<string> { roomId }, true, new List<int> { keycardNumber }, username));
                }
                else
                {
                    var currentName = userProfiles.Where(w => w.Room == roomId).Select(s => s.Name).FirstOrDefault();
                    tw.WriteLine($"Cannot book room {roomId} for {username}, The room is currently booked by {currentName}.");
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void Checkout(string keycardId, string username)
        {
            var user = userProfiles.FirstOrDefault(w => w.Keycard.ToString() == keycardId);
            if (user == null)
            {
                throw new ArgumentException($"Username or keycard not found.");
            }
            if (user.Name == username)
            {
                availableRooms.Add(user.Room);
                userProfiles.Remove(user);
                tw.WriteLine(Massage(new List<string> { user.Room }, false, null, null));
            }
            else
            {
                tw.WriteLine($"Only {user.Name} can checkout with keycard number {keycardId}.");
            }
        }

        private void GetListAvailableRooms()
        => tw.WriteLine(new StringBuilder().AppendJoin(", ", availableRooms).ToString());

        private void GetListGuest()
        => tw.WriteLine(new StringBuilder().AppendJoin(", ", userProfiles.Select(s => s.Name).Distinct()).ToString());

        private void GetGuestInRoom(string room)
       => tw.WriteLine(userProfiles.Where(f => f.Room == room).Select(s => s.Name).FirstOrDefault());

        private void GetListGuestByAge(string operatorSign, string age)
        => tw.WriteLine(OperatorCompare(operatorSign, age));

        private void GetListGuestByFloor(string floor)
        => tw.WriteLine(new StringBuilder().AppendJoin(", ", userProfiles.Where(s => s.Room[..1] == floor).Select(s => s.Name)).ToString());

        private void BookByFloor(string floor, string username, string strAge)
        {
            try
            {
                var age = int.Parse(strAge);
                var rooms = availableRooms.Where(w => w[..1] == floor).ToList();
                if (rooms.Count == initRoom)
                {
                    var keycards = new List<int>();
                    foreach (var room in rooms)
                    {
                        keycards.Add(BookUserProfile(room, username, age));
                    }
                    tw.WriteLine(Massage(rooms, true, keycards, null));
                }
                else
                {
                    tw.WriteLine($"Cannot book floor {floor} for {username}.");
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void CheckoutGuestByFloor(string floor)
        {
            var rooms = userProfiles.Where(s => s.Room[..1] == floor).Select(s => s.Room).ToList();
            availableRooms.AddRange(rooms);
            userProfiles.RemoveAll(r => rooms.Contains(r.Room));
            tw.WriteLine(Massage(rooms, false, null, null));
        }

        private int BookUserProfile(string num, string name, int age)
        {
            int keycardNumber = userProfiles.Count + 1;
            if (userProfiles.Count > 0 && userProfiles.Select(s => s.Keycard).Max() > userProfiles.Count)
            {
                var keycardList = userProfiles.Select(s => s.Keycard);
                keycardNumber = Enumerable.Range(1, userProfiles.Count).Except(keycardList).Min();
            }
            userProfiles.Add(new UserProfile { Age = age, Name = name, Room = num, Keycard = keycardNumber });
            availableRooms.Remove(num);
            return keycardNumber;
        }

        private string Massage(List<string> rooms, bool isBook, List<int> keycards, string name)
        {
            var r = new StringBuilder("Room ");
            r.AppendJoin(", ", rooms.OrderBy(o => o)).Append(rooms.Count > 1 ? " are " : " is ");
            if (isBook)
            {
                r.Append("booked ");
                if (name != null)
                {
                    r.Append($"by {name} ");
                }
                r.Append("with keycard number ");
                r.AppendJoin(", ", keycards.OrderBy(o => o));
                r.Append('.');
                return r.ToString();
            }
            r.Append("checkout.");
            return r.ToString();
        }

        private string OperatorCompare(string operatorSign, string strAge)
        {
            try
            {
                var age = int.Parse(strAge);
                var result = new StringBuilder();
                switch (operatorSign)
                {
                    case "<":
                        result.AppendJoin(", ", userProfiles.Where(w => w.Age < age).Select(s => s.Name).Distinct());
                        break;

                    case ">":
                        result.AppendJoin(", ", userProfiles.Where(w => w.Age > age).Select(s => s.Name).Distinct());
                        break;

                    default:
                        result.AppendJoin(", ", userProfiles.Where(w => w.Age != age).Select(s => s.Name).Distinct());
                        break;
                }
                if (operatorSign.Contains('='))
                {
                    result.AppendJoin(", ", userProfiles.Where(w => w.Age == age).Select(s => s.Name).Distinct());
                }
                return result.ToString();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}