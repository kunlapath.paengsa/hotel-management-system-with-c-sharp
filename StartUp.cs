﻿using hotel_management_system_with_c_sharp.Commons;
using System.Text;


namespace hotel_management_system_with_c_sharp
{
    public class StartUp
    {
        public void Run()
        {
            var content = ReadFileContent();
            new SystemService().GenerateFileResults(content);
        }

        private string[] ReadFileContent()
        => File.ReadAllLines(Common.GetFilePath("input.txt"), Encoding.UTF8);
    }
}
